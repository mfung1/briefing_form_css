const gulp = require('gulp'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglifycss'),
      sass = require('gulp-sass'),
      watch = require('gulp-watch'),
      autoprefixer = require('gulp-autoprefixer'),
      sourcemaps = require('gulp-sourcemaps'),
      browserSync = require('browser-sync').create(),
      SassOptions= {
        errLogToConsole: true,
        outputStyle: 'expanded'
      };

gulp.task('browser-sync', ['styles'], function () {
  browserSync.init({
    server: "../Briefing_Form/"
  });
});

gulp.task('watch', ['browser-sync'], function () {
  gulp.watch("sass/**/**/*.sass", ['styles']);
  gulp.watch("../Briefing_Form/index.html", [browserSync.reload]);
});

gulp.task('styles', function () {
  return gulp.src('sass/main.sass')
    .pipe(sourcemaps.init())
    .pipe(sass(SassOptions).on('error', sass.logError))

    .pipe(autoprefixer())
    .pipe(concat('main.min.css'))
    .pipe(uglify())
    .pipe(sourcemaps.write('../Briefing_Form/css'))
    .pipe(gulp.dest('../Briefing_Form/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('default', ["styles"])
