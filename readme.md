# Briefing Form CSS

>This repo is intended to host the development files for the styling on the form created by Dave, which can be located [here](https://bitbucket.org/d_baird/online_form)

## Note:

>The styling in this project is written entirely in **sass** not **scss**. If you need a refresher (works similarly to .scss and .less) check out:

* [Sass cheat sheet](https://codepen.io/mimoduo/post/sass-cheat-sheet)

# Setup

## Step 1
>To get started with this, you will need to first have Dave's repo cloned to your local, where the files in it are acessible as:

* ../Briefing_Form/

>If not, you will need to change the path in ./gulpfile.js to reflect the location of main.min.css in Dave's repo on lines:

* 34
* 35
* 16

## Step 2

>Then, when both parts are set up; open terminal and traverse to the directory typically this will be as follows:

	cd Documents/my_path_to_directory

>Once you are in this directory, run:

	npm i

>Note: You may need to run this under Sudo if you get errors (it will mention permission denied).

## Step 3

>Assuming you have everything installed, running gulp by default will just run 'gulp' for basic styling updates (it will concatenate and minify into the file in /css/main.min.css).

>If you run 'gulp watch' this will do the same as above, but will also launch BrowserSync. This is a npm package which allows you get live refreshes of code updates. It should launch it in your default browser, and will open 'localhost:XXXX'.

>It will also output the IP you can connect to the preview if you want to test on mobile (note you will need to be on the **exact** same network as your computer).

## Step 4

>The next step is just to explain the layout of the files structure.

>All .sass files must be referenced in:

	main.sass

>For them to be included in the minifcation process.

>You should split out the files into seperate folders / files for modularisation purposes, this will help with debugging.

>There is a sourcemaps plugin in this gulp stack but it is not writing correctly, so all styles may say it is from one file ( not the specific files like it is supposed to.)

>If you need to write a **media query**, you can do so by using:

	@ include breakpoint(size)

>(No spaces)

>Where **size** is either:

* mobile
* tablet
* laptop
* desktop

>These are breakpoints which can be found in './sass/mixins/media_queries.sass'.

>That should be about it! Happy coding!
